/*
  LiquidCrystal Library - Hello World

 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.

 This sketch prints "Hello World!" to the LCD
 and shows the time.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <LiquidCrystal.h>

// Pin definition for LCD contrast output
const int PIN_LCD_CONTRAST = 6;
const int CAL_LCD_CONTRAST = 115;

// Variables for moving average
const int NUM_VALS = 100;
int fifo[NUM_VALS];
int index = 0;
long acc = 0;
int movingAvg = 0;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(9, 8, 5, 4, 3, 2);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");

  // Initialize LCD contrast pin
//  pinMode(PIN_LCD_CONTRAST, OUTPUT);
  analogWrite(PIN_LCD_CONTRAST, CAL_LCD_CONTRAST);

  Serial.begin(9600);
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(millis() / 1000);

  // Compute moving average
  computeMovingAvg(analogRead(A0));

  

  String result = "A0=" + String(movingAvg) + "=" + String(movingAvg / 1024.0 * 5) + "V";

  lcd.setCursor(4, 1);
//  lcd.print("A0=");
//  lcd.print(movingAvg);
//  lcd.print("=");
//  lcd.print(movingAvg / 1024.0 * 5);
//  lcd.print("V        ");

  lcd.print(result);
  lcd.print("    ");
  Serial.print(millis());
  Serial.print(": ");
  Serial.println(result);
}

void serialEvent() {
  // Update LCD contrast
  int newContrast = Serial.parseInt();
  analogWrite(PIN_LCD_CONTRAST, newContrast);
  Serial.println("Updated LCD contrast to " + String(newContrast));
}

int computeMovingAvg(int newVal) {
  // Compute new sum
  acc = acc + newVal - fifo[index];

  // Shift FIFO
  fifo[index] = newVal;
  index = (index + 1) % NUM_VALS;

  // Compute moving average
  movingAvg = acc / NUM_VALS;
}

